package java_hw_client;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class Java_HW_client {

    private static PrintWriter socketout;
    private static BufferedReader socketin;
    private static Socket echoSocket;

    static JFrame frm1 = new JFrame("您是?");
    static JLabel lab1_1 = new JLabel();
    static JLabel lab1[] = new JLabel[2];
    static JButton btn0 = new JButton("註冊");
    static JTextField tex1[] = new JTextField[2];
    static JButton btn1 = new JButton("登入");
    private static String user = "", pass = "", getStr_temp = "", getStr = "";

    static Font font1 = new Font("標楷體", Font.CENTER_BASELINE, 20);
    static Font font2 = new Font("標楷體", Font.CENTER_BASELINE, 12);

    public static void main(String[] args) {
        try {
            echoSocket = new Socket("127.0.0.1", 12345);
            socketout = new PrintWriter(new OutputStreamWriter(echoSocket.getOutputStream(), "BIG5"));
            socketin = new BufferedReader(new InputStreamReader(echoSocket.getInputStream(), "BIG5"));
            Frame1();
        } catch (IOException ie) {
        }
    }

    private static void Frame1() {

        frm1.setVisible(true);
        frm1.setBounds(0, 0, 400, 400);
        frm1.setLayout(null);
        frm1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm1.setResizable(false);
        frm1.getContentPane().setBackground(Color.white);
        frm1.addWindowListener(new CLOSE_Lis());

        frm1.add(lab1_1);
        lab1_1.setBounds(75, 50, 350, 30);
        lab1_1.setFont(font1);
        lab1_1.setForeground(Color.red);
        lab1_1.setVisible(true);

        String temp1[] = {"帳號：", "密碼："};
        for (int i = 0; i < lab1.length; i++) {
            lab1[i] = new JLabel(temp1[i]);
            frm1.add(lab1[i]);
            lab1[i].setFont(font1);
            lab1[i].setBounds(75, 100 + 40 * i, 100, 30);
            lab1[i].setVisible(true);

            tex1[i] = new JTextField();
            frm1.add(tex1[i]);
            tex1[i].setFont(font1);
            tex1[i].setBounds(175, 100 + 40 * i, 100, 30);
            tex1[i].setVisible(true);
        }
        frm1.add(btn1);
        btn1.setBounds(75, 200, 100, 40);
        btn1.setVisible(true);
        btn1.addActionListener(new Lis1_2());
        tex1[1].addKeyListener(new Lis1_1());

        frm1.add(btn0);
        btn0.setBounds(215, 200, 100, 40);
        btn0.setVisible(true);
        btn0.addActionListener(new Lis1_3());

    }

    private static class Lis1_1 implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            login_pass(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            login_pass(e);
        }
    }

    private static void login_pass(KeyEvent e) {

        if (e.getKeyCode() == 8) {
            if (tex1[1].getText().length() == 0) {
                pass = "";
            } else {
                pass = pass.substring(0, tex1[1].getText().length());
            }
        } else if (tex1[1].getText().length() > 0) {
            String temp = tex1[1].getText();
            char temp2 = temp.charAt(temp.length() - 1);
            if ((temp2 == '*') && (pass.length() > temp.length())) {
                tex1[1].setText(temp.substring(0, temp.length() - 1));
            } else if ((pass.length() > temp.length()) && (temp.charAt(temp.length() - 1) != '*')) {
                pass = pass.substring(0, temp.length() - 1) + temp.charAt(temp.length() - 1);
                tex1[1].setText(temp.substring(0, temp.length() - 1) + "*");
            } else if ((temp2 >= 48 && temp2 <= 57) || (temp2 >= 65 && temp2 <= 90) || (temp2 >= 97 && temp2 <= 122)) {
                pass += temp2;
                tex1[1].setText(temp.substring(0, temp.length() - 1) + "*");
            } else if ((temp.charAt(temp.length() - 1) != '*')) {
                tex1[1].setText(temp.substring(0, temp.length() - 1));
            }
        } else if (tex1[1].getText().length() == 0) {
            pass = "";

        }
    }

    private static class Lis1_2 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            user = tex1[0].getText();
            try {
                getStr = socketin.readLine();
                /*user = "root";
                 pass = "root";*/
                socketout.println(user);
                socketout.flush();
                getStr = socketin.readLine();
                socketout.println(pass);
                socketout.flush();
                getStr = socketin.readLine();
                if (getStr.equals("login_success")) {
                    lab1_1.setText("");
                    frm1.setVisible(false);
                    frm2.setVisible(true);
                    Frame2();
                } else {
                    lab1_1.setText("帳密錯誤，請重新輸入");
                    user = "";
                    pass = "";
                    tex1[0].setText("");
                    tex1[1].setText("");
                }
            } catch (IOException ex) {
            }
        }

    }

    static JFrame frm2 = new JFrame("您是"+user);
    static JLabel lab2[] = new JLabel[8];
    static JLabel lab2_1 = new JLabel("請重新選擇");
    static JButton btn2[] = new JButton[8];
    static JButton btn2_1 = new JButton("新增房間");

    private static void Frame2() {
        frm2.setBounds(0, 0, 800, 800);
        frm2.setLayout(null);
        frm2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm2.setResizable(false);
        frm2.getContentPane().setBackground(Color.white);
        frm2.addWindowListener(new CLOSE_Lis());

        socketout.println("1-8");
        socketout.flush();
        for (int i = 0; i < lab2.length; i++) {
            try {
                lab2[i] = new JLabel(socketin.readLine());
            } catch (IOException ex) {
                lab2[i] = new JLabel("ERROR");
            }
            frm2.add(lab2[i]);
            lab2[i].setFont(font1);
            lab2[i].setBounds(200 + 250 * (i % 2), 50 + 150 * (i / 2), 150, 80);
            lab2[i].setVisible(true);
            btn2[i] = new JButton("進入房間");
            frm2.add(btn2[i]);
            btn2[i].setFont(font2);
            btn2[i].setBounds(330 + 250 * (i % 2), 130 + 150 * (i / 2), 100, 50);
            btn2[i].setVisible(true);
            btn2[i].addActionListener(new Lis2_1());
        }
        frm2.add(btn2_1);
        btn2_1.setFont(font2);
        btn2_1.setBounds(650, 700, 100, 50);
        btn2_1.setVisible(true);
        btn2_1.addActionListener(new Lis2_1());

        frm2.add(lab2_1);
        lab2_1.setFont(font1);
        lab2_1.setBounds(300, 0, 150, 50);
        lab2_1.setForeground(Color.red);
        lab2_1.setVisible(false);
    }

    private static class CLOSE_Lis implements WindowListener {

        @Override
        public void windowOpened(WindowEvent e) {
        }

        @Override
        public void windowClosing(WindowEvent e) {
            try {
                System.out.println("STOP");
                socketin.close();
                socketout.close();
                echoSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(Java_HW_client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void windowClosed(WindowEvent e) {
        }

        @Override
        public void windowIconified(WindowEvent e) {
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }
    }

    private static class Lis2_1 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String number[];
            String temp = "";

            try {
                if (btn2_1 == e.getSource()) {
                    frm2.setVisible(false);
                    Frame2_1();
                } else {
                    for (int i = 0; i < btn2.length; i++) {
                        if (btn2[i] == e.getSource()) {
                            number = lab2[i].getText().split(". ");
                            socketout.println(number[0]);
                            socketout.flush();
                        }
                    }
                    if (socketin.readLine().equals("link")) {
                        frm2.setVisible(false);
                        Frame4();
                    }
                }
            } catch (IOException ex) {
            }
        }
    }
    static JFrame frm2_1 = new JFrame("您是"+user);
    static JLabel lab2_1_1 = new JLabel("房間名稱：");
    static JTextField tex2_1_1 = new JTextField();
    static Choice cho2_1_1 = new Choice();
    static JLabel lab2_1_2 = new JLabel("遊戲類型：");
    static String gamemode[] = {"BlackJack", "gmae"};
    static String gamemodetype = "BlackJack";
    static int gamemode_total[] = {2, 8};
    static int peoplenum = 2;
    static JLabel lab2_1_3 = new JLabel();
    static JButton btn2_1_1 = new JButton("新增");

    private static void Frame2_1() {
        frm2_1.setBounds(0, 0, 400, 400);
        frm2_1.setLayout(null);
        frm2_1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm2_1.setResizable(false);
        frm2_1.getContentPane().setBackground(Color.white);
        frm2_1.addWindowListener(new CLOSE_Lis());
        frm2_1.setVisible(true);

        frm2_1.add(lab2_1_1);
        lab2_1_1.setFont(font1);
        lab2_1_1.setBounds(100, 70, 200, 30);
        lab2_1_1.setVisible(true);

        frm2_1.add(tex2_1_1);
        tex2_1_1.setFont(font1);
        tex2_1_1.setBounds(100, 100, 200, 30);
        tex2_1_1.setVisible(true);

        frm2_1.add(lab2_1_2);
        lab2_1_2.setFont(font1);
        lab2_1_2.setBounds(100, 150, 200, 30);
        lab2_1_2.setVisible(true);

        frm2_1.add(btn2_1_1);
        btn2_1_1.setFont(font2);
        btn2_1_1.setBounds(120, 250, 80, 50);
        btn2_1_1.setVisible(true);
        btn2_1_1.addActionListener(new Lis2_1_2());

        frm2_1.add(cho2_1_1);
        cho2_1_1.setBounds(100, 180, 100, 50);
        for (int i = 0; i < gamemode.length; i++) {
            cho2_1_1.add(gamemode[i]);
        }
        cho2_1_1.addMouseMotionListener(new Lis2_1_1());
        cho2_1_1.setVisible(true);

        frm2_1.add(lab2_1_3);
        lab2_1_3.setFont(font1);
        lab2_1_3.setBounds(100, 250, 200, 30);
        lab2_1_3.setVisible(true);

    }

    private static class Lis2_1_1 implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            for (int i = 0; i < gamemode.length; i++) {
                System.out.println();///**********************************************
                if (e.getID() == i) {
                    lab2_1_3.setText("遊戲人數：" + gamemode_total[i]);
                    peoplenum = gamemode_total[i];
                    gamemodetype = gamemode[i];
                }
            }
        }
    }

    private static class Lis2_1_2 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if (!tex2_1_1.getText().equals("")) {
                    String temp = "N-" + peoplenum + "-" + gamemodetype + "-" + tex2_1_1.getText();
                    socketout.println(temp);
                    socketout.flush();
                    if ((socketin.readLine()).equals("link")) {
                        frm2_1.setVisible(false);
                        Frame4();
                    }
                }
            } catch (IOException ex) {
            }
        }
    }

    //以下皆為註冊用
    private static class Lis1_3 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            frm3.setVisible(true);
            frm1.setVisible(false);
            frm2.setVisible(false);
            Frame3();
        }
    }

    static JFrame frm3 = new JFrame("您是"+user);

    static JLabel lab3[] = new JLabel[3];
    static JTextField tex3[] = new JTextField[3];
    static JButton btn3 = new JButton("送出");

    private static String user_n = "", pass_n = "", pass2_n = "", getStr_temp_n = "", getStr_n = "";

    private static void Frame3() {

        frm3.setVisible(true);
        frm3.setBounds(0, 0, 400, 400);
        frm3.setLayout(null);
        frm3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm3.setResizable(false);
        frm3.getContentPane().setBackground(Color.white);
        frm3.addWindowListener(new CLOSE_Lis());

        frm3.add(lab1_1);
        lab1_1.setBounds(75, 50, 250, 30);
        lab1_1.setFont(font1);
        lab1_1.setForeground(Color.red);
        lab1_1.setVisible(true);

        String temp1[] = {"帳號：", "密碼：", "再次輸入密碼:"};
        for (int i = 0; i < lab3.length; i++) {
            lab3[i] = new JLabel(temp1[i]);
            frm3.add(lab3[i]);
            lab3[i].setFont(font1);
            lab3[i].setBounds(40, 100 + 40 * i, 200, 30);
            lab3[i].setVisible(true);

            tex3[i] = new JTextField();
            frm3.add(tex3[i]);
            tex3[i].setFont(font1);
            tex3[i].setBounds(200, 100 + 40 * i, 100, 30);
            tex3[i].setVisible(true);
        }
        frm3.add(btn3);
        btn3.setBounds(125, 250, 100, 40);
        btn3.setVisible(true);
        btn3.addActionListener(new Lis1_4());
        tex3[1].addKeyListener(new Lis1_1_3());
        tex3[2].addKeyListener(new Lis1_1_3());
    }

    private static class Lis1_1_3 implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            login_pass2(e);
            login_pass3(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            login_pass2(e);
            login_pass3(e);
        }
    }

    private static void login_pass2(KeyEvent e) {

        if (e.getKeyCode() == 8) {
            if (tex3[1].getText().length() == 0) {
                pass_n = "";
            } else {
                pass_n = pass_n.substring(0, tex3[1].getText().length());
            }
        } else if (tex3[1].getText().length() > 0) {
            String temp = tex3[1].getText();
            char temp2 = temp.charAt(temp.length() - 1);
            if ((temp2 == '*') && (pass_n.length() > temp.length())) {
                tex3[1].setText(temp.substring(0, temp.length() - 1));
            } else if ((pass_n.length() > temp.length()) && (temp.charAt(temp.length() - 1) != '*')) {
                pass_n = pass_n.substring(0, temp.length() - 1) + temp.charAt(temp.length() - 1);
                tex3[1].setText(temp.substring(0, temp.length() - 1) + "*");
            } else if ((temp2 >= 48 && temp2 <= 57) || (temp2 >= 65 && temp2 <= 90) || (temp2 >= 97 && temp2 <= 122)) {
                pass_n += temp2;
                tex3[1].setText(temp.substring(0, temp.length() - 1) + "*");
            } else if ((temp.charAt(temp.length() - 1) != '*')) {
                tex3[1].setText(temp.substring(0, temp.length() - 1));
            }
        } else if (tex3[1].getText().length() == 0) {
            pass_n = "";

        }
    }

    private static void login_pass3(KeyEvent e) {

        if (e.getKeyCode() == 8) {
            if (tex3[2].getText().length() == 0) {
                pass2_n = "";
            } else {
                pass2_n = pass2_n.substring(0, tex3[2].getText().length());
            }
        } else if (tex3[2].getText().length() > 0) {
            String temp = tex3[2].getText();
            char temp2 = temp.charAt(temp.length() - 1);
            if ((temp2 == '*') && (pass2_n.length() > temp.length())) {
                tex3[2].setText(temp.substring(0, temp.length() - 1));
            } else if ((pass2_n.length() > temp.length()) && (temp.charAt(temp.length() - 1) != '*')) {
                pass2_n = pass2_n.substring(0, temp.length() - 1) + temp.charAt(temp.length() - 1);
                tex3[2].setText(temp.substring(0, temp.length() - 1) + "*");
            } else if ((temp2 >= 48 && temp2 <= 57) || (temp2 >= 65 && temp2 <= 90) || (temp2 >= 97 && temp2 <= 122)) {
                pass2_n += temp2;
                tex3[2].setText(temp.substring(0, temp.length() - 1) + "*");
            } else if ((temp.charAt(temp.length() - 1) != '*')) {
                tex3[2].setText(temp.substring(0, temp.length() - 1));
            }
        } else if (tex3[2].getText().length() == 0) {
            pass2_n = "";

        }
    }

    private static class Lis1_4 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (!pass_n.equals(pass2_n)) {
                lab1_1.setText("兩組密碼不相同");
                user_n = "";
                pass_n = "";
                pass2_n = "";
                tex3[0].setText("");
                tex3[1].setText("");
                tex3[2].setText("");
            } else {

                user_n = tex3[0].getText();
                try {
                    getStr_n = socketin.readLine();
                    socketout.println("/NEW");
                    socketout.flush();
                    getStr_n = socketin.readLine();
                    socketout.println(user_n);
                    socketout.flush();
                    getStr_n = socketin.readLine();
                    socketout.println(pass_n);
                    socketout.flush();
                    getStr_n = socketin.readLine();
                    if (getStr_n.equals("新增成功")) {
                        lab1_1.setText("");
                        frm1.setVisible(true);
                        frm2.setVisible(false);
                        frm3.setVisible(false);
                        Frame1();
                    } else {
                        lab1_1.setText("新增失敗");
                        user_n = "";
                        pass_n = "";
                        pass2_n = "";
                        tex3[0].setText("");
                        tex3[1].setText("");
                        tex3[2].setText("");

                    }
                } catch (IOException ex) {
                }
            }
        }

    }

    static JFrame frm4 = new JFrame("您是"+user);
    static JTextArea JTest4_1 = new JTextArea();
    static JLabel lab4_1 = new JLabel("房間名稱：");
    static JLabel lab4_2 = new JLabel("目前玩家");
    static JButton btn4 = new JButton("開始遊戲");
    static boolean bb = true;

    private static void Frame4() {
        try {
            String roomname = socketin.readLine();
            frm4.setVisible(true);
            frm4.setBounds(0, 0, 400, 400);
            frm4.setLayout(null);
            frm4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frm4.setResizable(false);
            frm4.getContentPane().setBackground(Color.white);
            frm4.addWindowListener(new CLOSE_Lis());

            frm4.add(lab4_1);
            lab4_1.setBounds(0, 0, 400, 100);
            lab4_1.setFont(new Font("標楷體", Font.BOLD, 20));
            lab4_1.setText(lab4_1.getText() + roomname);

            frm4.add(lab4_2);
            lab4_2.setBounds(0, 55, 100, 100);
            lab4_2.setFont(new Font("標楷體", Font.BOLD, 14));

            frm4.add(btn4);
            btn4.setBounds(250, 250, 100, 100);
            btn4.setFont(new Font("標楷體", Font.BOLD, 14));
            btn4.addActionListener(new ActLis4());

            frm4.add(JTest4_1);
            JTest4_1.setBackground(SystemColor.control);
            JTest4_1.setBounds(100, 100, 50, 240);
            JTest4_1.setFont(new Font("標楷體", Font.BOLD, 14));
            JTest4_1.setEnabled(false);
            String NAME_TEMP, NAME_TEMP_ALL = "";
            while (!(NAME_TEMP = socketin.readLine()).equals("NAMETT_STOP")) {
                if (!NAME_TEMP.equals("null")) {
                    NAME_TEMP_ALL += NAME_TEMP + "\n";
                }
            }
            JTest4_1.setText(NAME_TEMP_ALL);

            final long timeInterval = 1000;
            Runnable runnable = new Runnable() {

                public void run() {
                    while (bb) {
                        // ------- code for task to run
                        // ------- ends here
                        try {
                            Thread.sleep(timeInterval);
                            String NAME_TEMP, NAME_TEMP_ALL = "";
                            while (socketin.ready()) {
                                while (!((NAME_TEMP = socketin.readLine()).equals("NAMETT_STOP"))) {
                                    if (NAME_TEMP.equals("game")) {
                                        bb = false;
                                        frm4.setVisible(false);
                                        Frame5();
                                        break;
                                    }
                                    if (NAME_TEMP.equals("null")) {
                                        break;
                                    }
                                    NAME_TEMP_ALL += NAME_TEMP + "\n";
                                }
                                if (bb) {
                                    JTest4_1.setText(NAME_TEMP_ALL);
                                }
                            }
                        } catch (InterruptedException e) {
                        } catch (IOException ex) {
                        }
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();

        } catch (IOException ex) {
        }
    }

    private static class ActLis4 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            socketout.println("game");
            socketout.flush();
            /*if (socketin.readLine().equals("game")) {
                    System.out.println("1");
                    frm4.setVisible(false);
                    Frame5();
                }*/
        }
    }
    static JFrame frm5 = new JFrame("您是"+user);
    static int playthis[] = new int[8];
    static int playthat[] = new int[8];
    static JLabel thislab = new JLabel();
    static int thismath, thatmath;
    static JLabel thatlab = new JLabel();
    static JLabel lab5_play1[] = new JLabel[8];//牌組圖片
    static JLabel lab5_play2[] = new JLabel[8];//牌組圖片
    static ImageIcon background = new ImageIcon(".\\POKE\\BG.jpg");//背景
    static ImageIcon pi[] = new ImageIcon[53];
    static JLabel bg = new JLabel(background);//背景
    static JButton btn5 = new JButton("開始遊戲");
    static JButton btn5_2 = new JButton("再抽一張");
    static JButton btn5_3 = new JButton("結算");
    static int math = 0;
    static JLabel win = new JLabel();

    private static void Frame5() {
        frm5.setBounds(0, 0, 800, 600);
        frm5.setLayout(null);
        frm5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm5.setResizable(false);
        frm5.getContentPane().setBackground(Color.white);
        frm5.addWindowListener(new CLOSE_Lis());
        frm5.setVisible(true);

        frm5.add(win);
        win.setFont(new Font("標楷體", Font.BOLD, 60));
        win.setBounds(0, 0, 800, 600);
        win.setVisible(false);

        for (int i = 0; i < 8; i++) {
            playthis[i] = 0;
            playthat[i] = 0;
        }
        pi[0] = new ImageIcon(".\\POKE\\0.png");
        for (int i = 1; i < 53; i++) {
            pi[i] = new ImageIcon(".\\POKE\\" + i + ".jpg");
        }
        for (int i = 0; i < lab5_play1.length; i++) {
            lab5_play1[i] = new JLabel();       //設定label和牌面圖片
            lab5_play1[i].setIcon(pi[0]);
            frm5.add(lab5_play1[i]);
            lab5_play1[i].setBounds(20 + 95 * i, 30, 94, 136);
            lab5_play1[i].setVisible(false);
        }

        for (int i = 0; i < lab5_play2.length; i++) {
            lab5_play2[i] = new JLabel();         //設定label和牌面圖片
            lab5_play2[i].setIcon(pi[0]);
            frm5.add(lab5_play2[i]);
            lab5_play2[i].setBounds(20 + 95 * i, 400, 94, 136);
            lab5_play2[i].setVisible(false);
            lab5_play2[i].addMouseListener(new MouLis5_1());
        }

        frm5.add(btn5);
        btn5.setBounds(50, 220, 130, 130);
        btn5.addActionListener(new ActLis5());

        frm5.add(btn5_2);
        btn5_2.setBounds(220, 220, 130, 130);
        btn5_2.addActionListener(new ActLis5());
        btn5_2.setVisible(false);

        frm5.add(btn5_3);
        btn5_3.setBounds(390, 220, 130, 130);
        btn5_3.addActionListener(new ActLis5());
        btn5_3.setVisible(false);

        frm5.add(thislab);
        thislab.setFont(new Font("標楷體", Font.BOLD, 20));
        thislab.setBounds(220, 376, 40, 40);
        thislab.setVisible(true);

        frm5.add(thatlab);
        thatlab.setFont(new Font("標楷體", Font.BOLD, 20));
        thatlab.setBounds(220, 186, 40, 40);
        thatlab.setVisible(true);

        final long timeInterval = 1000;
        Runnable runnable = new Runnable() {

            String mathtemp[];

            public void run() {
                String temp;
                while (true) {
                    // ------- code for task to run
                    // ------- ends here
                    try {
                        Thread.sleep(timeInterval);
                        while (socketin.ready()) {
                            temp = socketin.readLine();
                            if (temp.equals("start")) {
                                btn5.setVisible(false);
                                btn5_2.setVisible(true);
                                btn5_3.setVisible(true);
                                playthis[0] = Integer.valueOf(socketin.readLine());
                                playthis[1] = Integer.valueOf(socketin.readLine());
                                playthat[0] = Integer.valueOf(socketin.readLine());
                                playthat[1] = Integer.valueOf(socketin.readLine());
                                lab5_play2[0].setIcon(pi[playthis[0]]);
                                lab5_play1[0].setIcon(pi[playthat[0]]);
                                add_math();
                            } else if (temp.equals("one")) {
                                String mathtemp1 = socketin.readLine();
                                mathtemp = mathtemp1.split("----");
                                for (int i = 0; i < 8; i++) {
                                    playthis[i] = Integer.valueOf(mathtemp[i]);
                                }
                                String mathtemp2 = socketin.readLine();
                                mathtemp = mathtemp2.split("----");
                                for (int i = 0; i < 8; i++) {
                                    playthat[i] = Integer.valueOf(mathtemp[i]);
                                    if ((!lab5_play1[i].isVisible()) && playthat[i] != 0) {
                                        lab5_play1[i].setVisible(true);
                                    }
                                }
                            } else if (temp.equals("end")) {
                                String mathtemp1 = socketin.readLine();
                                mathtemp = mathtemp1.split("----");
                                for (int i = 0; i < 8; i++) {
                                    playthis[i] = Integer.valueOf(mathtemp[i]);
                                }
                                String mathtemp2 = socketin.readLine();
                                System.out.println(temp);
                                mathtemp = mathtemp2.split("----");
                                for (int i = 0; i < 8; i++) {
                                    playthat[i] = Integer.valueOf(mathtemp[i]);
                                    if (playthat[i] != 0) {
                                        lab5_play1[i].setVisible(true);
                                        lab5_play1[i].setIcon(pi[playthat[i]]);
                                    }
                                }

                                thatmath = Integer.valueOf(socketin.readLine());
                                thatlab.setText(thatmath + "");
                                win.setText(socketin.readLine());
                                win.setVisible(true);
                            }
                        }
                    } catch (InterruptedException ex) {
                    } catch (IOException ex) {
                    }
                    for (int i = 0; i < 8; i++) {
                        if (playthis[i] != 0) {
                            lab5_play2[i].setVisible(true);
                        }
                        if (playthat[i] != 0) {
                            lab5_play1[i].setVisible(true);
                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private static class MouLis5_1 implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            for (int i = 1; i < 8; i++) {
                if (e.getSource() == lab5_play2[i]) {
                    lab5_play2[i].setIcon(pi[playthis[i]]);
                }
            }
            add_math();
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

    }

    private static class ActLis5 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btn5) {
                socketout.println("start");
                socketout.flush();
            } else if (e.getSource() == btn5_2) {
                socketout.println("one");
                socketout.flush();
            } else if (e.getSource() == btn5_3) {
                socketout.println("end");
                socketout.println(thislab.getText());
                socketout.flush();
                btn5_2.setVisible(false);
                btn5_3.setVisible(false);
            }
        }
    }

    public static void add_math() {
        math = 0;
        int mathTT = 0, mathTA, Aone = 0;
        int ma[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10};
        for (int i = 0; i < playthis.length; i++) {
            if (playthis[i] >= 5 && lab5_play2[i].isVisible()) {
                mathTT += ma[((playthis[i] - 1) / 4) + 1];
            } else if (playthis[i] >= 1 && playthis[i] <= 4 && lab5_play2[i].isVisible()) {
                Aone++;
            }
        }
        int AAone[] = {11, 10, 1};
        int AAtwo[] = {21, 20, 12, 11, 2};
        int AAthree[] = {21, 13, 12, 3};
        int AAfour[] = {14, 13, 4};
        if (Aone == 1) {
            for (int i = 0; i < AAone.length; i++) {
                if (mathTT + AAone[i] <= 21) {
                    math = mathTT + AAone[i];
                    break;
                }
                if (i == AAone.length - 1) {
                    math = mathTT + AAone[i];
                    break;
                }
            }
        } else if (Aone == 2) {
            for (int i = 0; i < AAtwo.length; i++) {
                if (mathTT + AAtwo[i] <= 21) {
                    math = mathTT + AAtwo[i];
                    break;
                }
                if (i == AAone.length - 1) {
                    math = mathTT + AAone[i];
                    break;
                }
            }
        } else if (Aone == 3) {
            for (int i = 0; i < AAthree.length; i++) {
                if (mathTT + AAthree[i] <= 21) {
                    math = mathTT + AAthree[i];
                    break;
                }
                if (i == AAone.length - 1) {
                    math = mathTT + AAone[i];
                    break;
                }
            }
        } else if (Aone == 4) {
            for (int i = 0; i < AAfour.length; i++) {
                if (mathTT + AAfour[i] <= 21) {
                    math = mathTT + AAfour[i];
                    break;
                }
                if (i == AAone.length - 1) {
                    math = mathTT + AAone[i];
                    break;
                }
            }
        } else {
            math = mathTT;
        }
        thislab.setText(math + "");
    }

}
